<?php
namespace Omnipay\BaoKim;

/**
 * Bao Kim Pro Class
 */
class ProGateway extends ExpressGateway
{

    public function getName()
    {
        return 'BaoKim Pro';
    }


    public function purchase(array $parameters = [ ])
    {
        return $this->createRequest('\Omnipay\BaoKim\Message\ProPurchaseRequest', $parameters);
    }


    public function completePurchase(array $parameters = [ ])
    {
        return $this->createRequest('\Omnipay\BaoKim\Message\ProCompletePurchaseRequest', $parameters);
    }

}